# PLSQL / ORACLE DB TAsk Additional Comments

### Function 6 & 7 : My preferred implementation for Function 6 & 7 and 8 using CURSOR

- Create an object type as table
```
CREATE TYPE JOINED_EMP_MGR AS TABLE OF (
  EMPLOYEE VARCHAR2(10),
  EMP# NUMBER(4),
  MANAGER VARCHAR2(10),
  MGR# NUMBER(4)
);

CREATE OR REPLACE TYPE JOINED_EMP_MGR AS tABLE OF JOINED_EMP_MGR_TABLE;
``` 
- Create Function that returns the datatype
```
--FUNCTION6
FUNCTION  F_HR_MANAGER RETURN JOINED_EMP_MGR_TABLE IS
    CURSOR hr_report IS
    SELECT e.ename AS Employee,
            e.empno AS Emp#,
            m.ename AS Manager,
            m.mgr AS Mgr#
    FROM emp e
    JOIN emp m ON e.mgr = m.empno;
    BEGIN
    OPEN hr_report;
    RETURN hr_report;
    END  F_HR_MANAGER; 
```

```
--FUNCTION7
FUNCTION F_HR_QUERY RETURN SYS_REFCURSOR IS
    CURSOR c IS
    SELECT ename, hiredate
    FROM emp
    WHERE hiredate > (SELECT hiredate FROM emp WHERE ename = 'JONES');
    BEGIN
    OPEN c;
    RETURN c;
    END F_HR_QUERY;
```

```
--FUNCTION8
FUNCTION F_SAL_ROUND
RETURN TABLE(
    name VARCHAR2(255),
    salary NUMBER(10,3)
) IS
BEGIN
    RETURN (
    SELECT ename || ' ' AS name, ROUND(sal,3) AS salary
    FROM emp;
    );
END F_SAL_ROUND;
```