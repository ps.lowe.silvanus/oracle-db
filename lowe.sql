CREATE OR REPLACE PACKAGE 
LOWE
IS
    
     --PROCEDURE1
    PROCEDURE P_PRINT_HELLO(P_MESSAGE VARCHAR2);
    
    --PROCEDURE2
    PROCEDURE P_CREATE_TAB_DEPT;
    
    --PROCEDURE3
    PROCEDURE P_CREATE_TAB_EMP;
    
    --PROCEDURE4
    PROCEDURE P_INSERT_DATA(
            P_EMPNO EMP.EMPNO%TYPE,
            P_ENAME EMP.ENAME%TYPE, 
            P_JOB EMP.JOB%TYPE, 
            P_MGR EMP.MGR%TYPE, 
            P_HIREDATE EMP.HIREDATE%TYPE, 
            P_SAL EMP.SAL%TYPE, 
            P_COMM EMP.COMM%TYPE, 
            P_DEPTNO DEPT.DEPTNO%TYPE
          
        );
    PROCEDURE P_INSERT_DEPT_DATA(
            P_DEPTNO DEPT.DEPTNO%TYPE, 
            P_DNAME DEPT.DNAME%TYPE, 
            P_LOC DEPT.LOC%TYPE
        );
    
    --PROCEDURE5
    PROCEDURE P_PRINT_EMP_SAL (
        P_ENAME EMP.ENAME%TYPE,
        P_DNAME DEPT.DNAME%TYPE
    );
    
    --PROCEDURE6
    PROCEDURE P_UPDATE_SAL (
        P_EMPNO EMP.EMPNO%TYPE,
        P_SAL EMP.SAL%TYPE
    );
    
     --PROCEDURE7
     PROCEDURE P_SQL_FUN;
     
     --PROCEDURE8
     PROCEDURE P_DELETE_USER(
        table_name IN VARCHAR2,
        table_no IN VARCHAR2,
        P_NO DEPT.DEPTNO%TYPE
    );
    
    --FUNCTION1
    FUNCTION F_DEPT_STAT(
        STAT_ID IN NUMBER,
        P_DEPT_ID IN NUMBER
    ) RETURN NUMBER;
    
    --FUNCTION2
    FUNCTION F_DEPT10_HR(department_id IN NUMBER, salary IN NUMBER)
    RETURN VARCHAR2;
    
    --FUNCTION3
    FUNCTION F_EMP_HR(p_output out varchar2) RETURN VARCHAR2;
    
    --FUNCTION4
    FUNCTION F_HR_Supervisor(p_output out varchar2) RETURN VARCHAR2;
    
    --FUNCTION5
    FUNCTION F_HR_REPORT RETURN NUMBER;    
    
    --FUNCTION6
    FUNCTION  F_HR_MANAGER RETURN NUMBER;
    
    --FUNCTION7
    FUNCTION F_HR_QUERY RETURN VARCHAR2;
    
    --FUNCTION8
    FUNCTION F_SAL_ROUND RETURN NUMBER;
END LOWE;
/

CREATE OR REPLACE PACKAGE BODY
LOWE
IS
     --PROCEDURE1
    PROCEDURE P_PRINT_HELLO(
    P_MESSAGE IN VARCHAR2
    ) 
    IS
    BEGIN
      DBMS_OUTPUT.put_line ('Hello World! ' || p_message);
    END P_PRINT_HELLO;

    --PROCEDURE2
    PROCEDURE P_CREATE_TAB_EMP IS
    BEGIN
      EXECUTE IMMEDIATE 'CREATE TABLE EMP
      (
        EMPNO NUMBER(4) NOT NULL,
        ENAME VARCHAR2(10) NOT NULL,
        JOB VARCHAR2(9),
        MGR NUMBER(4),
        HIREDATE DATE,
        SAL NUMBER(7,2),
        COMM NUMBER(7,2),
        DEPTNO NUMBER(2),
        CONSTRAINT PK_EMP PRIMARY KEY (EMPNO),
        CONSTRAINT FK_DEPT
            FOREIGN KEY (DEPTNO)
            REFERENCES DEPT (DEPTNO)
      )';
    END P_CREATE_TAB_EMP;

    --PROCEDURE3
    PROCEDURE P_CREATE_TAB_DEPT IS
   begin
     EXECUTE IMMEDIATE 'CREATE TABLE DEPT
     (
     DEPTNO  NUMBER(2) NOT NULL PRIMARY KEY, 
     DNAME  VARCHAR2(14) NOT NULL UNIQUE, 
     LOC    VARCHAR2(13)
     )';
   end P_CREATE_TAB_DEPT;
    
    --PROCEDURE4
    PROCEDURE P_INSERT_DATA(
        P_EMPNO EMP.EMPNO%TYPE,
        P_ENAME EMP.ENAME%TYPE, 
        P_JOB EMP.JOB%TYPE, 
        P_MGR EMP.MGR%TYPE, 
        P_HIREDATE EMP.HIREDATE%TYPE, 
        P_SAL EMP.SAL%TYPE, 
        P_COMM EMP.COMM%TYPE, 
        P_DEPTNO DEPT.DEPTNO%TYPE
    ) IS 
    BEGIN
      INSERT INTO EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) VALUES (P_EMPNO, P_ENAME, P_JOB, P_MGR, P_HIREDATE, P_SAL, P_COMM, P_DEPTNO);
    END P_INSERT_DATA;
    
    PROCEDURE P_INSERT_DEPT_DATA(
        P_DEPTNO DEPT.DEPTNO%TYPE,
        P_DNAME DEPT.DNAME%TYPE,
        P_LOC DEPT.LOC%TYPE
    ) IS 
    BEGIN
      INSERT INTO DEPT (DEPTNO, DNAME, LOC) VALUES (P_DEPTNO, P_DNAME, P_LOC);
    END P_INSERT_DEPT_DATA;
    
    --PROCEDURE5
    PROCEDURE P_PRINT_EMP_SAL (
        P_ENAME EMP.ENAME%TYPE,
        P_DNAME DEPT.DNAME%TYPE
    ) IS
    P_SAL NUMBER;
    BEGIN
      SELECT SAL INTO P_SAL
      FROM EMP
      WHERE ENAME = P_ENAME
      AND DEPTNO = (SELECT DEPTNO FROM DEPT WHERE DNAME = P_DNAME);
    
      DBMS_OUTPUT.PUT_LINE('Salary for Employee ' || P_ENAME || ' from department ' || P_DNAME || ' = ' || P_SAL);
    END P_PRINT_EMP_SAL;
    
    --PROCEDURE6
        PROCEDURE P_UPDATE_SAL (
        P_EMPNO EMP.EMPNO%TYPE,
        P_SAL EMP.SAL%TYPE
        ) IS
        BEGIN
            UPDATE EMP
            SET SAL = P_SAL
            WHERE EMPNO = P_EMPNO;
        END P_UPDATE_SAL;
        
     --PROCEDURE7
    PROCEDURE P_SQL_FUN
       IS
       V_ENAME EMP.ENAME%TYPE;
       V_STAR  EMP.SAL%TYPE; 
       V_CHAR  VARCHAR2(50);
       BEGIN
       dbms_output.put_line('EMPLOYEE_AND_THEIR_SALARIES');
       dbms_output.put_line('----------------------------');
       FOR EMP_REC IN(SELECT ENAME, SAL FROM EMP ORDER BY emp.sal DESC)
        LOOP
        V_ENAME := EMP_REC.ENAME;
        V_STAR := TRUNC(EMP_REC.SAL / 100);
        V_CHAR := RPAD('*', V_STAR, '*');
        dbms_output.put_line(V_ENAME || '    ' || V_CHAR); 
        END LOOP;  
    end P_SQL_FUN;
      
    --PROCEDURE8
        PROCEDURE P_DELETE_USER(
        table_name IN VARCHAR2,
        table_no IN VARCHAR2,
        P_NO DEPT.DEPTNO%TYPE
        ) IS
          BEGIN
            EXECUTE IMMEDIATE 'DELETE FROM ' || table_name || ' WHERE ' || table_no || ' = ' || P_NO;
          END P_DELETE_USER;
      
      
    --FUNCTION1
        FUNCTION F_DEPT_STAT(
        STAT_ID IN NUMBER,
        P_DEPT_ID IN NUMBER
        ) RETURN NUMBER 
        IS
        result NUMBER;
          BEGIN
            IF stat_id = 1 THEN
                SELECT COUNT(*) INTO result FROM EMP WHERE EMP.DEPTNO = P_DEPT_ID;
            ELSIF stat_id = 2 THEN
                SELECT AVG(sal) INTO result FROM EMP WHERE EMP.DEPTNO = P_DEPT_ID;
            ELSE
                RAISE_APPLICATION_ERROR(-20001, 'Invalid variable value');
            END IF;
            RETURN result;
          END F_DEPT_STAT;

 --FUNCTION2
    FUNCTION F_DEPT10_HR(
      department_id IN NUMBER, 
      salary IN NUMBER
    ) RETURN VARCHAR2 
    IS
      employee_info VARCHAR2(2000) := ' ';
      emp_no NUMBER;
      emp_dept NUMBER;
      emp_salary NUMBER;
      emp_name VARCHAR2(50); -- Specify the appropriate length for emp_name.
      
      CURSOR employees IS
        SELECT EMPNO, ENAME, DEPTNO, SAL
        FROM EMP
        WHERE EMP.DEPTNO = department_id OR EMP.SAL > salary;
    BEGIN
      OPEN employees;
      LOOP
        FETCH employees INTO emp_no, emp_name, emp_dept, emp_salary;
        EXIT WHEN employees%NOTFOUND;
    
        employee_info := employee_info || emp_no || ' ' || emp_name || ' ' || emp_dept || CHR(10);
      END LOOP;
    
      CLOSE employees;
      RETURN employee_info;
    END F_DEPT10_HR;
  
    --FUNCTION3
      FUNCTION F_EMP_HR(p_output out varchar2) RETURN VARCHAR2
        AS
                 V_ENAME EMP.ENAME%TYPE;
                 V_JOB EMP.JOB%TYPE;
                 V_SAL EMP.SAL%TYPE;
                 V_DEPTNO DEPT.DEPTNO%TYPE;
                 V_RESULT VARCHAR(500);
        begin
          FOR EMP_REC IN(SELECT ENAME, JOB, SAL, DEPTNO FROM EMP WHERE  emp.sal < 4000 OR emp.sal > 6000)
          LOOP
          V_ENAME := EMP_REC.ENAME;
          V_JOB := EMP_REC.JOB;
          V_SAL := EMP_REC.SAL;
          V_DEPTNO := EMP_REC.DEPTNO;
          V_RESULT := 'Employee ' || V_ENAME || ' has Job ' || V_JOB || ' salary = ' || V_SAL || ' in department ' || V_DEPTNO ;      
          dbms_output.put_line(V_RESULT);
          END LOOP;
        p_output := V_RESULT;
        RETURN p_output;
        end F_EMP_HR;
        
    --FUCNTION4    
      FUNCTION F_HR_Supervisor(p_output out varchar2) RETURN VARCHAR2
      AS
      V_COUNT NUMBER;
      V_SAL NUMBER;
      V_MGR NUMBER;
      V_RESULT VARCHAR2(500);
      BEGIN
         FOR EMP_REC IN (SELECT emp.mgr, COUNT(emp.empno) AS empcount , max(sal) AS maxsal  FROM EMP GROUP BY  emp.mgr)
         LOOP
          V_COUNT := EMP_REC.empcount;
          V_SAL := EMP_REC.maxsal;
          V_MGR := EMP_REC.MGR;
          V_RESULT := 'Manager:' || V_MGR || ', Supervisees:' || V_COUNT || ' employees, Highest salary:' || V_SAL;
          dbms_output.put_line(V_RESULT);
         END LOOP;
       p_output := V_RESULT;
       RETURN p_output;
      end F_HR_Supervisor;
        
    --FUNCTION5     
      FUNCTION F_HR_REPORT RETURN NUMBER
      AS
      V_HIREDATE EMP.HIREDATE%TYPE;
      V_TODAY DATE;
      V_MONTHS NUMBER;
      V_EMPNO EMP.EMPNO%TYPE;
      V_RDATE DATE;
      V_LDAY DATE;
      V_FRIDAY DATE;
      begin
        SELECT SYSDATE INTO V_TODAY FROM DUAL;
        FOR EMP_REC IN(SELECT HIREDATE, EMPNO FROM EMP)
        LOOP
          V_EMPNO := EMP_REC.EMPNO;
          V_HIREDATE := TO_DATE(EMP_REC.HIREDATE, 'DD-MON-RR');
          SELECT TRUNC(MONTHS_BETWEEN(V_TODAY,V_HIREDATE)) INTO V_MONTHS FROM DUAL;
          IF V_MONTHS > 500 THEN
          V_RDATE := ADD_MONTHS(V_HIREDATE, 6);
          V_LDAY := LAST_DAY(V_HIREDATE);
          V_FRIDAY := NEXT_DAY(V_HIREDATE,'FRIDAY');
          dbms_output.put_line('EmployeeNo:' || V_EMPNO || ' hireDate:' || V_HIREDATE || ' NoOfMonthsEmployed:' || V_MONTHS || ' reviewDate:' || V_RDATE || ' firstFriday:' || V_FRIDAY || ' lastDay:' || V_LDAY);
          end if;
        END LOOP;
        RETURN V_MONTHS;
      end F_HR_REPORT;        
        
    --FUNCTION6   
    FUNCTION  F_HR_MANAGER RETURN NUMBER IS
    CURSOR hr_report IS
      SELECT e.ename AS Employee,
             e.empno AS Emp#,
             m.ename AS Manager,
             m.mgr AS Mgr#
        FROM emp e
        JOIN emp m ON e.mgr = m.empno;
      BEGIN
        OPEN hr_report;
       
      END  F_HR_MANAGER; 
        
    --FUNCTION7
     FUNCTION F_HR_QUERY RETURN VARCHAR2
      AS
      V_HIREJONES EMP.HIREDATE%TYPE;
      V_DAYS NUMBER;
      begin 
        SELECT hiredate INTO V_HIREJONES from EMP WHERE empno = 7566;
        FOR EMP_REC IN(SELECT HIREDATE, ENAME FROM EMP)
        LOOP
          V_DAYS := (TO_DATE(EMP_REC.HIREDATE, 'DD-MON-RR') - TO_DATE(V_HIREJONES, 'DD-MON-RR'));      
        if V_DAYS > 0 THEN
        dbms_output.put_line(EMP_REC.ENAME || ' hired after jones on the:' || EMP_REC.HIREDATE);
        end if;
        END LOOP;
       end F_HR_QUERY;
       
    --FUNCTION8
    FUNCTION F_SAL_ROUND RETURN NUMBER
      AS
      V_SAL EMP.SAL%TYPE;
      begin
      FOR EMP_REC IN(SELECT ENAME, SAL FROM EMP)
      LOOP
        V_SAL := ROUND(EMP_REC.SAL,-3);
        dbms_output.put_line(EMP_REC.ENAME || ', salary:' || V_SAL);
      END LOOP;
      return V_SAL;
      END F_SAL_ROUND;
END LOWE;
/


--Question 4: database link for a user called lowe and running on the same localhost at ort 1521
CREATE DATABASE LINK lowedb
CONNECT TO lowe IDENTIFIED BY lowe
USING '(
        DESCRIPTION=
        (ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=1521))
        (CONNECT_DATA=(SERVICE_NAME=lowe)))';
        
SELECT * FROM dept@lowedb; 
    
--Question 5
CREATE MATERIALIZED VIEW employee_report AS
SELECT e.deptno AS DEPARTMENT, e.ename AS EMPLOYEE, c.ename AS COLLEAGUE
    FROM emp e
    JOIN emp c ON e.deptno = c.deptno;
 -- Generate report from material view for user FORD   
SELECT department, employee, colleague
FROM employee_report
WHERE employee = 'FORD'
ORDER BY employee, colleague;
    
--Question 6
BEGIN 
    DBMS_SCHEDULER.CREATE_JOB 
    (
        JOB_NAME            => 'refresh_materialized_view_job',
        JOB_TYPE            => 'PLSQL_BLOCK',
        JOB_ACTION          => 'BEGIN REFRESH MATERIALIZED VIEW employee_report; END;',
        NUMBER_OF_ARGUMENTS => 0,
        START_DATE          => SYSTIMESTAMP,
        REPEAT_INTERVAL     => 'FREQ=MINUTELY; INTERVAL=5; BYHOUR=0; BYDAY=MON,TUE,WED,THU,SUN',
        END_DATE            => NULL,
        ENABLED             => TRUE,
        AUTO_DROP           => FALSE,
        COMMENTS            => 'JOB TO REFRESH hr_report Materialized View'
    );
END;


--Question 7
CREATE TABLE ps_orders( 
    id number(5), 
    quantity number(4), 
    cost_per_item number(6,2), 
    total_cost number(8,2), 
    create_date date, 
    created_by varchar2(10),
    update_date date,
    updated_by varchar2(10)
    );

-- create sequence  that increments by 1 from 1  
CREATE SEQUENCE order_seq
START WITH 1
INCREMENT BY 1;

--create id trigger
CREATE TRIGGER identity_no_trigger
BEFORE INSERT ON ps_orders
FOR EACH ROW
BEGIN
  SELECT order_seq.NEXTVAL INTO :new.id;
END;
 
--create date trigger  
CREATE TRIGGER date_trigger
BEFORE INSERT ON ps_orders
FOR EACH ROW
BEGIN
  :new.create_date := SYSDATE;
END;

--create trigger for created_by
CREATE TRIGGER created_by_trigger
BEFORE INSERT ON ps_orders
FOR EACH ROW
BEGIN
  :new.created_by := USER;
END;

--create trigger for tototal cost calculation
CREATE OR REPLACE TRIGGER calculate_total_cost_trigger
BEFORE INSERT OR UPDATE ON ps_orders
FOR EACH ROW
BEGIN
  :new.total_cost := :new.quantity * :new.cost_per_item;
END;

--create trigger for update auditor
CREATE OR REPLACE TRIGGER update_auditor_trigger
BEFORE UPDATE ON ps_orders
FOR EACH ROW
BEGIN
  :new.update_date := SYSDATE;
  :new.updated_by := USER;
END;

--insert a row with only quantity and  cost per item fields and this will trigger all the created triggers except the update trigger
INSERT INTO ps_orders (quantity, cost_per_item) VALUES (5, 2);

--update cost per item. this will trigger the update auditor trigger
UPDATE ps_orders SET cost_per_item = 5 where id = 1;

--to query the ROW
SELECT * FROM ps_orders;

--Q8
CREATE TABLE employees (
  employee_id NUMBER(10) PRIMARY KEY,
  last_name VARCHAR2(255) NOT NULL,
  department_id NUMBER(10) NOT NULL,
  hire_date DATE NOT NULL
);

-- Create a B-Tree index on the last_name column
CREATE INDEX idx_employees_last_name ON employees (last_name);

-- Create a bitmap index on the department_id column
CREATE BITMAP INDEX idx_employees_department_id ON employees (department_id);

-- Create a unique index on the hire_date column
CREATE UNIQUE INDEX idx_employees_hire_date ON employees (hire_date);

-- Create a function-based index on the last_name column
CREATE INDEX idx_employees_last_name_upper ON employees (UPPER(last_name));

-- Create a concatenated index on the last_name and department_id columns
CREATE INDEX idx_employees_last_name_department_id ON employees (last_name, department_id);
